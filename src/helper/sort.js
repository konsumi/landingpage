/**
 * function to sort an array of object by
 * a specific property
 */
module.exports = {
  sortBy: function(obj, property) {
    obj.sort((a, b) => (a[property] > b[property]) ? 1 : -1);
    return obj;
  }
}