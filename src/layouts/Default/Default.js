var m = require("mithril");

var Header = require("../../components/Header/Header");
var Footer = require("../../components/Footer/Footer");

module.exports = {
  view: function (vnode) {
    return m(
      "main.k-app flex-grow font-inter antialiased bg-gray-900 text-gray-200 tracking-tight",
      [
        m(".flex flex-col overflow-hidden", m(Header)),
        m("main.flex-grow", [
          m("section", vnode.children)
        ]),
        m(Footer),
      ]
    );
  },
};
