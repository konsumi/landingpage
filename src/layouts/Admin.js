var m = require("mithril")

var Navigation = require("./components/Navigation")

module.exports = {
    view: function(vnode) {
        return m("main.k-app", [
            m("h1", "konsumi"),
            m(Navigation),
            m("section", vnode.children)
        ])
    }
}