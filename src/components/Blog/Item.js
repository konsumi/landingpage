var m = require("mithril")

module.exports = {
  view: function(vnode) {
    return m("div.flex flex-col h-full", [
      m("header", [
        m("a.block mb-6", [
          m("figure.relative h-0 pb-9/16 overflow-hidden rounded-sm", [
            m("img.absolute inset-0 w-full h-full object-cover transform hover:scale-105 transition duration-700 ease-out", { src: vnode.attrs.image, width: 352, height: 198, alt: vnode.attrs.title })
          ])
        ]),
        m("div.mb-3", [
          m("ul.flex flex-wrap text-xs font-medium -m-1",
            m("li.m-1", vnode.attrs.tags.map(function(tag) {
              return m("a.inline-flex text-center text-gray-100 py-1 px-3 rounded-full bg-purple-600 hover:bg-purple-700 transition duration-150 ease-in-out " + tag.classes, tag.name)
            }))
          )
        ]),
        m("h3.h4 mb-2", [
          m("a.hover:text-gray-100 transition duration-150 ease-in-out", { onClick: vnode.attrs.clickAction}, vnode.attrs.title)
        ])
      ]),
      m("p.text-lg text-gray-400 flex-grow", vnode.attrs.description),
      m("footer.flex items-center mt-4", [
        m("a", { onClick: vnode.attrs.clickAction }, [
          m("img.rounded-full flex-schrink-0 mr-4", { src: vnode.attrs.author.image, width: 40, height: 40, alt: vnode.attrs.author.name })
        ]),
        m("div.font-medium", [
          m("a.text-gray-200 hover:text-gray-100 transition duration-150 ease-in-out", vnode.attrs.author.name),
          m("span.text-gray-700", " - "),
          m("span.text-gray-500", vnode.attrs.date)
        ])
      ])
    ])
  }
}