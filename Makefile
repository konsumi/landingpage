NODEVER := v16.17.1
NPM ?= PATH="$(CURDIR)/node-$(NODEVER)/bin:$$PATH" npm
VERSION ?= $(shell git log -1 --pretty=format:'%h - %cD')

help: Makefile
	@sed -n 's/^##//p' $<

## run          serves the front end
run: prepare
	$(NPM) run serve

##
## content      generates our data streams
content: .phatfilecms
	@$(MAKE) DST=../content -B -s -C $< SETTINGS="$(PWD)/.phatfilecms.py" all

## cms          serves our phatfilecms webend
cms: .phatfilecms
	@$(MAKE) DST=../content -s -C $< SETTINGS="$(PWD)/.phatfilecms.py" run

##
## build        generates the distribution package
build: prepare
	$(NPM) run $@

##
## prepare      installs all dependencies
prepare: node_modules content

node_modules: node-$(NODEVER) package.json
	$(NPM) ci
	@echo Signature: 8a477f597d28d172789f06886806bc55 > $@/CACHEDIR.TAG
	@touch $@

node-$(NODEVER):
	@mkdir -p $@
	curl -s -L https://nodejs.org/dist/$(NODEVER)/node-$(NODEVER)-linux-x64.tar.gz \
		| tar xzf - --strip-components=1 -C $@
	@echo Signature: 8a477f597d28d172789f06886806bc55 > $@/CACHEDIR.TAG

## npm-XXX      runs npm XXX command
npm-%:
	-$(NPM) $*

.phatfilecms: .phatfilecms/config.mk
	git submodule update --init
	@touch $@

.phatfilecms/config.mk: .phatfilecms.mk
	@mkdir -p $(@D)
	@ln -rsf $< $@


##
## clean        removes all generated output
clean:
	@rm -rf -- dist content
	@-git checkout -f package-lock.json

## distclean    purges all generated output and dependencies
distclean: clean
	@rm -rf -- node_modules node-* .phatfilecms

.PHONY: run clean distclean prepare build content cms
