# landingpage

[![build](https://ci.codeberg.org/api/badges/konsumi/landingpage/status.svg?branch=main)](https://ci.codeberg.org/konsumi/landingpage)

First konsumi public page to transport our vision

## usage

to create the content from phatfilecms execute
```
make clean content
```

to run the dev server execute
```
npm run serve
```

## submodules

By executing `git submodule add https://codeberg.org/konsumi/phatfilecms "./.phatfilecms"`, the `.phatfilecms` is added to the project to provide the content data.