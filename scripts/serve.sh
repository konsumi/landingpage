#!/usr/bin/bash
DIST_DIR=$(pwd)"/dist";
PUBLIC_STATIC_DIR=./public

# remove dist folder and create new
echo "Clean up destination folder"
if [ -d "$DIST_DIR" ]; then
  rm -r "$DIST_DIR"
fi
mkdir "$DIST_DIR"
# copy static files from assets
cp -r "$PUBLIC_STATIC_DIR"/* "$DIST_DIR"

# esbuild commands
echo "Esbuild"
node ./scripts/serve/serve.js
