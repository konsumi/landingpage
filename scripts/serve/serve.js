const path = require("path");
const { sassPlugin } = require("esbuild-sass-plugin");
const autoprefixer = require("autoprefixer");
const tailwindcss = require("tailwindcss");
const postcss = require("postcss");
const { createServer, request } = require("http");
const { spawn } = require("child_process");
const esbuild = require("esbuild");

const clients = [];
const ENTRY_FILE = "src/index.js";
const OUT_FILE = "dist/index.js";
const MODE = process.env["NODE_ENV"] || "production";
const TARGET = "es5";

esbuild
  .build(
    {
      entryPoints: [ENTRY_FILE],
      outfile: OUT_FILE,
      target: TARGET,
      bundle: true,
      minify: false,
      logLevel: "info",
      sourcemap: true,
      loader: {
        ".png": "file",
        ".html": "text",
        ".ttf": "file",
        ".otf": "file",
        ".svg": "file",
        ".eot": "file",
        ".woff": "file",
        ".woff2": "file"
      },
      plugins: [
        sassPlugin({
          async transform(source, resolveDir) {
            const { css } = await postcss(
              autoprefixer,
              tailwindcss(path.resolve(__dirname, "./../../tailwind.config.js"))
            ).process(source);
            return css;
          },
        }),
      ],
      banner: {
        js: ' (() => new EventSource("http://localhost:3000/esbuild").onmessage = () => location.reload())();',
      },
      define: {
        "process.env.NODE_ENV":
          MODE === "dev" || MODE === "development"
            ? '"development"'
            : '"production"',
        global: "window",
      },
      watch: {
        onRebuild(error, result) {
          clients.forEach((res) => res.write("data: update\n\n"));
          clients.length = 0;
          console.log(error ? error : "...");

          if (error) console.error("watch build failed:", error);
          else console.log("watch build succeeded:", result);
        },
      },
    }
  )
  .then(() => {
    console.log(`[+] Esbuild ${ENTRY_FILE} to ${OUT_FILE} succeeded.`);
  })
  .catch((e) => {
    console.log("[-] Error building:", e.message);
    process.exit(1);
  });

esbuild.serve({ servedir: "./dist", host: "localhost" }, {}).then(() => {
  createServer((req, res) => {
    const { url, method, headers } = req;
    if (req.url === "/esbuild")
      return clients.push(
        res.writeHead(200, {
          "Content-Type": "text/event-stream",
          "Cache-Control": "no-cache",
          'Access-Control-Allow-Origin': 'localhost:8000',
          Connection: "keep-alive",
        })
      );
    const path = ~url.split("/").pop().indexOf(".") ? url : ``;
    req.pipe(
      request(
        { hostname: "0.0.0.0", port: 8000, path, method, headers },
        (prxRes) => {
          res.writeHead(prxRes.statusCode, prxRes.headers);
          prxRes.pipe(res, { end: true });
        }
      ),
      { end: true }
    );
  }).listen(3000);

  setTimeout(() => {
    const op = {
      darwin: ["open"],
      linux: ["xdg-open"],
      win32: ["cmd", "/c", "start"],
    };
    const ptf = process.platform;
    if (clients.length === 0)
      spawn(op[ptf][0], [...[op[ptf].slice(1)], `http://localhost:3000`]);
  }, 1000);
});
