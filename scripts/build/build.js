const path = require("path");
const { sassPlugin } = require("esbuild-sass-plugin");
const autoprefixer = require("autoprefixer");
const tailwindcss = require("tailwindcss");
const postcss = require("postcss");
const ENTRY_FILE = "src/index.js";
const OUT_FILE = "dist/index.js";
const MODE = process.env["NODE_ENV"] || "production";
const TARGET = "es5";


require('esbuild').build({
  bundle: true,
  entryPoints: [ENTRY_FILE],
  outfile: OUT_FILE,
  target: TARGET,
  bundle: true,
  minify: false,
  logLevel: "info",
  sourcemap: true,
  loader: {
    ".png": "file",
    ".html": "text",
    ".ttf": "file",
    ".otf": "file",
    ".svg": "file",
    ".eot": "file",
    ".woff": "file",
    ".woff2": "file"
  },
  plugins: [
    sassPlugin({
      async transform(source, resolveDir) {
        const { css } = await postcss(
          autoprefixer,
          tailwindcss(path.resolve(__dirname, "./../../tailwind.config.js"))
        ).process(source);
        return css;
      },
    }),
  ],
  define: {
    "process.env.NODE_ENV":
      MODE === "dev" || MODE === "development"
        ? '"development"'
        : '"production"',
    global: "window",
  },
}).catch(() => process.exit(1))