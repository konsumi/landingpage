#!/usr/bin/env bash
BASE_PATH="../../";

cd $BASE_PATH;
while IFS="" read -r p; do
   if [ -n "$(echo "$p" | rg -F node_modules)" ]; then
      continue
   fi

   cd "$p" > /dev/null || exit
   cwd="$(pwd)"
   result="$(npm ls -s | rg "(coa@(2\.0\.3|2\.0\.4|2\.1\.1|2\.1\.3|3\.0\.1|3\.1\.3)|rc@(1\.2\.9|1\.3\.9|2\.3\.9))")"
   cd - > /dev/null || exit

   if [ -n "$result" ]; then
      echo -e "$cwd\n$result\n" >> check.txt
   fi
done < <(fd -iF "package.json" -x echo {//})